--luacheck: read globals Connector Encoder Conveyor Script Timer EncoderData

-----------------------------------------------------------------------------------------------
-- variable
-----------------------------------------------------------------------------------------------

local encoderTable = {
  Connect = nil, -- handle
  Data = nil, -- handle
  GetValueTimer = nil, -- Timer handler
  PORT_POWER = 'INC1',
  PORT_RECEIVE = 'ENC1',
  Power = nil, -- handle
  increment = 0, -- last increment value
  distancePerIncrement = 1, -- distance[mm] / increment  (need to research)
  SET_RECEIVE_TIME = 1000 -- receive encoder data frequency
}

encoderTable.GetValueTimer = Timer.create()
encoderTable.GetValueTimer:setPeriodic(true)
encoderTable.GetValueTimer:setExpirationTime(encoderTable.SET_RECEIVE_TIME)

encoderSpeed = 0


--Start of Function and Event Scope---------------------------------------------

-----------------------------------------------------------------------------------------------
-- Setting
-----------------------------------------------------------------------------------------------

-- power on
local function settingEncoder()
  encoderTable.Power = Connector.Power.create(encoderTable.PORT_POWER)
  encoderTable.Power:enable(true)
end

-----------------------------------------------------------------------------------------------
-- Connect
-----------------------------------------------------------------------------------------------

function connectEncoder()

  encoderTable.Connect = Encoder.create(encoderTable.PORT_RECEIVE)

  ---[[
  Conveyor.setSource(encoderTable.PORT_RECEIVE)
  Conveyor.setResolution(10000)
  Conveyor.setIncrementUpdateTimeout(3000)
  Conveyor.setCurrentIncrement(0)
  --]]

  print('connectEncoder() ' .. tostring(encoderTable.Connect))

  encoderTable.GetValueTimer:start()

end


-----------------------------------------------------------------------------------------------
-- Measuring
-----------------------------------------------------------------------------------------------

local function calcConveyorSpeed(_increment)
  local incrementPerSec =  _increment - encoderTable.increment
  if incrementPerSec < 0 then
    incrementPerSec = incrementPerSec + 0x10000
  end
  encoderSpeed = incrementPerSec * encoderTable.distancePerIncrement
  encoderTable.increment = _increment
  print("calcConveyorSpeed() speed = ".. encoderSpeed)
end

-----------------------------------------------------------------------------------------------
-- OnReceive (Timer)
-----------------------------------------------------------------------------------------------

local function onReceiveEncoder()
  local increment,
    timestamp = encoderTable.Connect:getCurrentIncrement()
  local direction = encoderTable.Connect:getDirection() -- return direction

  -- need to research Encoder's hardware. it maybe need to set parameters(ex: speed) or can't get speed.
  local speed = encoderTable.Connect:getTicksPerSecond()

  print("----------------------------------------------------")
  print('ENC1 -> Increment = ' .. increment .. ', Direction = ' ..
      direction .. ', Speed = ' .. speed .. ', timestamp = ' .. timestamp)

  local convIncr = Conveyor.getCurrentIncrement()
  local convSpeed = Conveyor.getSpeed()

  local distance = Conveyor.incrementToDistance(convIncr)

  print('CONV -> Increment = ' .. convIncr .. ', Speed = ' .. convSpeed .. ", Distance = ".. distance)

  calcConveyorSpeed(convIncr)

end
encoderTable.GetValueTimer:register('OnExpired', onReceiveEncoder)

-- reference
-- https://supportportal.sick.com/tutorial/sim4000-getting-started/


-----------------------------------------------------------------------------------------------
-- Main
-----------------------------------------------------------------------------------------------

settingEncoder()

local function main()
  -- write app code in local scope, using API
  connectEncoder()
end
Script.register("Engine.OnStarted", main)
-- serve API in global scope
